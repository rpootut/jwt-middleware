<?php

namespace App\Http\Middleware;

use Closure;

class BasicMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headers = $request->headers->get('authorization');

        if($headers != 'edgar'){
            return 'nel';
        }

        return $next($request);
    }
}
