<?php

namespace App\Http\Middleware;

use Closure;
use \Firebase\JWT\JWT;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $jwt = str_replace('Bearer ', '', $request->headers->get('authorization'));
        
        $rawKeys = file_get_contents('https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com');
        
        $publicKeys = json_decode($rawKeys, true);

        $decoded = JWT::decode($jwt, $publicKeys, array('RS256'));

        //print_r($decoded);

        return $next($request);
    }
}
